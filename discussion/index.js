// JSON Objects
/*
	Syntax:
		{
			"propertyA": "valueA",
			"propertyB": "valueB"
		}
*/

/*
	{
		"city": "Quezon City",
		"province": "Metro Manila",
		"country": "Philippines"
	}
*/

/*
	JSON Arrays

	"cities": [
		{"city": "Quezon city", "province": "Metro Manila", "country": "Philippines"},
		{"city": "Manila city", "province": "Metro Manila", "country": "Philippines"},
		{"city": "Makati city", "province": "Metro Manila", "country": "Philippines"}	
	]
*/

// JSON Methods

// Converting Data into Stringified JSON

let batchesArr = [{ batchName: 'Batch X' },{ batchName: 'Batch Y' }];

console.log('Result from stringify method:');
console.log(JSON.stringify(batchesArr));

// Converting Data directly from an object

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
})

console.log(data);

// Using stringify method with variables
/*
	Syntax:
		JSON.stringify({
			propertyA: variableA,
			propertyB: variableB
		})
*/

// User details
let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('Waht is your age?');
let address = {
	city: prompt('Which city do you live in?'),
	country: prompt('Which country does your city address belong to?')
};

let otherData = JSON.stringify({
	firstname: firstName,
	lastname: lastName,
	Age: age,
	Address: address

})

console.log(otherData);

// Converting Stringified JSON intor JavaScript Objects

let batchesJSON = `[{"batchName": "Batch X"},{"batchName": "Batch Y"}]`;

console.log('Result from parse method; ');
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{ "name": "John", "age": "31", "address": { "city": "Manila", "country": "Philippines" } }`;

console.log(JSON.parse(stringifiedObject));